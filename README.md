# event-contract

# Maven Coordinates
## Repository
Emerjoin open source software repository
```xml
<repository>
    <id>emerjoin</id>
    <name>oss</name>
    <url>https://pkg.emerjoin.org/oss</url>
</repository>
```

## Artifact
Latest version
```xml
<dependency>
    <groupId>org.emerjoin.annotations</groupId>
    <artifactId>event-contract-annotations</artifactId>
    <version>1.0.1-GA</version>
</dependency>
```

# User Manual
## Event emissions
Indicating that an event is expected to be emitted.
### Type
```java
    @Emits(MyEvent1.class)
    @Emits(MyEvent2.class)
    public interface MyComponent {
        
    }
```

### Method
```java
    
    public interface MyComponent {
    
        @Emits(MyEvent1.class)
        @Emits(MyEvent2.class)
        void action();
        
    }
```

## Event watching
Indicating that an event is expected to be watched. This annotation is only available on type level.

### Type
```java
    
    @Watches(MyEvent1.class)
    @Watches(MyEvent2.class)
    public interface MyComponent {
        
    }
```

