package org.emerjoin.event.contract;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
@Repeatable(EmittedEvents.class)
public @interface Emits {
    Class<?> value();
}
