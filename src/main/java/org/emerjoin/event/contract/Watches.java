package org.emerjoin.event.contract;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Repeatable(WatchedEvents.class)
public @interface Watches {
    Class<?> value();
}
